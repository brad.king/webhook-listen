// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

extern crate iron;
use self::iron::{Handler, IronResult, Request, Response};
use self::iron::method::Method;
use self::iron::status;

extern crate serde_json;
use self::serde_json::Value;

use super::config::Config;

use std::io::{Error, ErrorKind, Read};
use std::path::{Path, PathBuf};
use std::sync::RwLock;

pub struct Router {
    path: PathBuf,
    config: RwLock<Config>,
}

impl Router {
    pub fn new<P: AsRef<Path>>(path: P, config: Config) -> Self {
        Router {
            path: path.as_ref().to_path_buf(),

            config: RwLock::new(config),
        }
    }

    fn reload(&self) -> Response {
        match Config::from_path(&self.path) {
            Ok(config) => {
                let mut inner_config = self.config.write().unwrap();

                *inner_config = config;

                Response::with(status::Ok)
            },
            Err(err) => Response::with((status::NotAcceptable, format!("{:?}", err))),
        }
    }

    fn handle(&self, path: &str, object: Value) -> Response {
        let config = self.config.read().unwrap();

        if let Some(handler) = config.handlers.get(path) {
            if let Some(kind) = handler.kind(&object) {
                if let Err(err) = handler.write_object(kind, object) {
                    error!(target: "handler",
                           "failed to write the {} object: {:?}",
                           kind,
                           err);
                }
            }

            Response::with(status::Accepted)
        } else {
            Response::with(status::NotFound)
        }
    }
}

impl Handler for Router {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!(target: "handler",
               "got a {} request at {:?}",
               req.method,
               req.url.path());

        Ok(match req.method {
            Method::Put => {
                if req.url.path() == ["__reload"] {
                    self.reload()
                } else {
                    Response::with(status::NotFound)
                }
            },
            Method::Post => {
                let path = req.url.path().join("/");
                let mut data = String::new();

                req.body.read_to_string(&mut data)
                    .and_then(|_| {
                        serde_json::from_str(&data)
                            .map_err(|err| {
                                Error::new(ErrorKind::Other, format!("{:?}", err))
                            })
                    })
                    .map(|object| {
                        self.handle(&path, object)
                    })
                    .unwrap_or_else(|err| {
                        Response::with((status::BadRequest, format!("{:?}", err)))
                    })
            },
            _ => Response::with(status::MethodNotAllowed),
        })
    }
}
